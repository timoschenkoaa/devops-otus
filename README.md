# Проект DevOps Otus

Для проекта выбрано микросервисное приложение Crawler
![Image text](./app.jpg)
![Image text](./app-2.jpg)

## Ссылки на развернутые ресурсы:
* https://crawler-ui.51.250.92.226.nip.io/ - web-Интерфейс Crawler
* https://crawler.51.250.92.226.nip.io/ - backend Crawler
* https://grafana.51.250.92.226.nip.io/ - Grafana 
> (login/pass: admin/prom-operator)
* https://prometheus.51.250.92.226.nip.io/ - Prometheus
* https://kibana.51.250.92.226.nip.io/ - Kibana (визуализация логов)
* https://kibana.51.250.92.226.nip.io/goto/c96d6179e63c6a0b357a83a65a29c565 - Kibana Board для Crawler

## Инфраструктура

* Для развертывания приложения используется Managed kubernetes cluster в Yandex.Cloud. Для развертывания кластера используется terraform  и Helm. Для доступа к сервисам извне задеплоен Traefik Ingress
* Для SSL установлен менеджер сертификатов cert-manager, сертификаты генерируются через манифесты. 
* Организован сбор и визуализация метрик (Prometheus + Grafana), логирование (ElasticSearch + Fluentd + Kibana).
* Кластер kubernetes интегрирован с GitLab через gitlab-agent, пайплайны выполняеются на gitlab-runner, развернутом в том же кластере. 
* Для автоматизированной доставки приложения в кластер,задеплоен FluxCD и созданы Helm-чарты для микросервисов crawler и crawler-ui

## Структура репозитория

*   **Корень репозитория**
    * .gitlab-ci.yaml - описание пайплайна gitlab
    * README.md - общая информация
* **config** - конфигурация terraform (в зависимости от системы меняется путь расположения файла)
* **deploy** - самописанный Helm-чарт микросервисов crawler и crawler-ui  
* **docker** - докер файлы для создания образов микросервисов
* **k8s-cluster** - terraform манифесты для развертывания и настройки кластера Kubernetes. Имеет свой README
* **k8s-gitlab**  - terraform манифесты для развертывания gitlab-agent и gitlab-runner
* **k8s-infra** - terraform манифесты для развертывания окружения: Traefik Ingress, Cert-Manager, FluxCD, Prometheus, Grafana,      
                  MongoDB, RabbitMQ, Kibana, Elasticsearch, FluentD. Имеет свой README

## Как запустить проект

1. Развернуть инфраструктуру из директории k8s-cluster по README находящемуся внутри каталога
2. Развернуть инфраструктуру из директории k8s-gitlab по README находящемуся внутри каталога
3. Развернуть инфраструктуру из директории k8s-infra по README находящемуся внутри каталога

После выполнения 3-х пунктов имеем готовый Kubernetes кластер в yandex.cloud, сервисы для мониторинга и логирования, доступ к сервисам извне по 443 порту, базу данных Mongo, RabbitMQ

4. В GitLab запустить пайплайн из ветки **main** для сборки и разворачивания микросервисов в кластере
