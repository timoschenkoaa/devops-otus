# Деплой кластера Kubernetes c помощью Terraform


## Требования

- Установленный и настроенный на облако Yandex Cloud CLI (>=0.86.0)
- Установленный terraform (>= 1.1.9) с установленной конфигурацией провайдера yandex для terraform.
  Конфигурация находится в проекте /config/terraform.rc. Куда положить конфиг, смотреть в доках (https://www.terraform.io/cli/config/), ибо для Windows/Linux есть 
- Установленная утилита для общения с kubernetes - kubectl


## Что делает проект

- Создает сеть и подсеть для кластера k8s
- Создает service account для YC для развертывания, присваивает необходимые роли
- Создает Managed cluster for Kubernetes, Worker nodes для кластера
- Прописывает необходимые настройки для доступа к кластеру в конфиг файл kubernetes (по умолчанию ~/.kube/config) и устанавливает контекст по умолчанию на созданный кластер

###Запуск проекта

Выполняем инициализацию terraform в текущей директории
```
    terraform init --backend-config=s3.tfbackend
```
Запускаем развертывание кластера. Настройки переменных указаны в файле terraform.tfvars.
Пример файла - terraform.tfvars.example

```
    terraform apply
```

## Проверка развертывания

Можно выполнить любую команду утилитой kubectl (настраивается автоматически в процессе разворачивания кластера), например
```
 kubectl get ns
```
Команда должна вернуть результат без ошибок
