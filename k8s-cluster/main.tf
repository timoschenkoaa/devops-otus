terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }

  required_version = ">= 0.13"

  # backend "s3" {
  #   endpoint   = "storage.yandexcloud.net"
  #   bucket     = "bucket18052022"
  #   region     = "ru-central1"
  #   key        = "terraform.tfstate"
  #   access_key = "YCAJEQSnLGhSRQ78Er2RivUTe"
  #   secret_key = "YCNuHQD_JY-KwnDXTUiRz8TPJjtCGSXxBNXzgE-0"
  #   skip_region_validation      = true
  #   skip_credentials_validation = true  
  # } # Заполняется из файла s3.tfbackend при инициализации: terraform init --backend-config=s3.tfbackend
}

provider "yandex" {
  token     = var.yandex_token
  cloud_id  = var.yandex_cloud_id
  folder_id = var.yandex_folder_id
  zone      = "ru-central1-a"
}

# Кластер kubernetes
resource "yandex_kubernetes_cluster" "kube_cluster" {
  name       = var.kube_cluster_name
  network_id = yandex_vpc_network.kube_net.id
  master {
    version = "1.20"
    zonal {
      zone      = yandex_vpc_subnet.kube_subnet.zone
      subnet_id = yandex_vpc_subnet.kube_subnet.id
    }
    public_ip = true
  }

  service_account_id      = yandex_iam_service_account.kube_account.id
  node_service_account_id = yandex_iam_service_account.kube_account.id
  depends_on              = [yandex_resourcemanager_folder_iam_binding.editor, yandex_resourcemanager_folder_iam_binding.images-puller]

  # Настройка config файла kubernetes для дальнейшего развертывания компонентов на созданный кластер
  provisioner "local-exec" {
    command = "yc managed-kubernetes cluster get-credentials ${self.name} --external --force"
  }
}

# Worker ноды
resource "yandex_kubernetes_node_group" "kube_nodes" {
  name       = var.kube_nodes_name
  cluster_id = yandex_kubernetes_cluster.kube_cluster.id
  version    = "1.20"

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat        = true
      subnet_ids = [yandex_vpc_subnet.kube_subnet.id]
    }

    resources {
      memory = 8
      cores  = 2
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }
  }

  scale_policy {
    fixed_scale { size = var.kube_nodes_count }
  }
}

# Сеть для кластера
resource "yandex_vpc_network" "kube_net" {
  name = var.kube_net_name
}

# Подсеть для кластера
resource "yandex_vpc_subnet" "kube_subnet" {
  name           = var.kube_subnet_name
  v4_cidr_blocks = ["10.2.0.0/24"]
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.kube_net.id
}

# Сервисный аккаунт для кластера
resource "yandex_iam_service_account" "kube_account" {
  name = var.kube_service_account_name
}

# Назначение ролей сервисному аккаунту

resource "yandex_resourcemanager_folder_iam_binding" "editor" {
  # Сервисному аккаунту назначается роль "editor".
  folder_id = var.yandex_folder_id
  role      = "editor"
  members   = ["serviceAccount:${yandex_iam_service_account.kube_account.id}"]
}
resource "yandex_resourcemanager_folder_iam_binding" "images-puller" {
  # Сервисному аккаунту назначается роль "container-registry.images.puller".
  folder_id = var.yandex_folder_id
  role      = "container-registry.images.puller"
  members   = ["serviceAccount:${yandex_iam_service_account.kube_account.id}"]
}
