variable "yandex_token" {
  type        = string
  description = "OAuth токен для доступа к yandex cloud"
}

variable "yandex_cloud_id" {
  type = string
}

variable "yandex_folder_id" {
  type = string
}


variable "kube_cluster_name" {
  type    = string
  default = "kube-cluster"
}

variable "kube_nodes_name" {
  type    = string
  default = "kube-nodes"
}
variable "kube_nodes_count" {
  type        = number
  default     = 2
  description = "Количество worker нод кластера"
}

variable "kube_net_name" {
  type    = string
  default = "kube-net"
}

variable "kube_subnet_name" {
  type    = string
  default = "kube-subnet"
}

variable "kube_service_account_name" {
  type    = string
  default = "kube-account"
}
