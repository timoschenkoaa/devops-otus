# Деплой gitlab-agent/gitlab-runner в кластере kubernetes с помощью Terraform

- gitlab-agent - для возможности развертывания приложений в кластере из gitlab
- gitlab-runner - для запуска пайплайнов gitlab

## Требования к окружению

- Установленный terraform (>= 1.1.9)
- Созданный кластер kubernetes, где будут развернуты компоненты
- Установленная утилита для общения с kubernetes - kubectl c настроенным на кластер контекстом
- Установленный helm (>= 3.8.2)
- Созданный проект в GitLab (gitlab.com)
- Данные для регистрации агента - можно получить при регистрации агента в gitlab:

> Infrastructure -> Kubernetes cluster -> Agent -> Connect the cluster (Agent) -> create

Нужные поля - config.token и config.kasAddress

- Данные для регистрации раннера

> Settings -> CI/CD -> Runners -> Specific runners

Нужен Url для регистрации и registration token.

Полученные данные вносим в terraform.tfvars по примеру 
> terraform.tfvars.example


## Что делает проект

- Создает namespace gitlab в kubernetes
- Разворачивает в созданном namespace gitlab-agent и gitlab-runner.
- Регистрирует компоненты в gitlab

## Запуск проекта

Выполняем инициализацию terraform в текущей директории
```
  terraform init
```

Запускаем развертывание компонентов. Настройки переменных указаны в файле terraform.tfvars.
Пример файла - terraform.tfvars.example
```
 terraform apply --auto-approve
```


### Проверка корректности развертывания

Проверить наличие развернутых подов

> kubectl get pod -n gitlab

Команда должна вернуть два развернутых пода.

Для проверки регистрации проверить статус раннера в gitlab (должен быть зеленый)

> Settings -> CI/CD -> Runners -> Specific runners

Проверить регистрацию агента (Connection status должен быть connected)

> Infrastructure -> Kubernetes cluster -> Agent