provider "helm" {
  kubernetes {
    config_path = var.kube_config_path
  }
}

provider "kubernetes" {
  config_path = var.kube_config_path
}

# Создание неймспейса для компонентов gitlab
resource "kubernetes_namespace" "gitlab" {
  metadata {
    name = "gitlab"
  }
}

# Установка gitlab-agent для деплоя приложения в k8s из gitlab
resource "helm_release" "gitlab-agent" {

  name = "gitlab-agent"
  repository = "https://charts.gitlab.io"
  chart      = "gitlab-agent"
  namespace = kubernetes_namespace.gitlab.metadata[0].name
  set {
    name  = "config.token"
    value = var.gitlab_agent_token
  }
  set {
    name  = "config.kasAddress"
    value = "wss://kas.gitlab.com"
  }
}

# Разворачивание и регистрация gitlab-runner для запуска пайплайнов в gitlab
resource "helm_release" "gitlab-runner" {

  name = "gitlab-runner"
  repository =  "https://charts.gitlab.io"
  chart      = "gitlab-runner"
  namespace = kubernetes_namespace.gitlab.metadata[0].name

  set {
    name  = "gitlabUrl"
    value = var.gitlab_url
  }
  set {
    name  = "runnerRegistrationToken"
    value = var.gitlab_runner_token
  }
  set {
    name  = "rbac.create"
    value = "true"
  }
    set {
        name  = "runners.privileged"
        value = "true"
    }
}
