# Деплой сервисов  в кластере kubernetes с помощью Terraform

- Prometheus + Grafana - для сбора метрик
- Elasticsearch + Fluentd + Kibana - для сбора логов
- Traefik Ingress - мощный ингресс контроллер для публикации сервисов наружу
- Cert-Manager - менеджер сертификатов. Создаем валидные сертификаты для доступа к сервисам по SSL
- FluxCD - отслеживает состояние кластера и репозитория, в данном проекте, может автоматически деплоить в кластер новую верстю приложения
- MongoDB + RabbitMQ - необходимы для работы приложения

## Требования к окружению

- Установленный terraform (>= 1.1.9)
- Созданный кластер kubernetes, где будут развернуты компоненты
- Установленная утилита для общения с kubernetes - kubectl c настроенным на кластер контекстом
- Установленный helm (>= 3.8.2)

## Что делает проект

- Создает в kubernetes следующие namspaces:

>  applications, cert-manager, flux, logging, monitoring, traefik

- Деплоит выше описанные сервисы с помощью Helm 

## Запуск проекта

Выполняем инициализацию terraform в текущей директории

```
  terraform init
```

Запускаем развертывание компонентов. Настройки переменных указаны в файле terraform.tfvars.
Пример файла - terraform.tfvars.example
``
terraform apply --auto-approve
```

Вытаскиваем ssh ключ Flux и добавляем его в настройках SSH профиля в GitLab (без него не будет работать)
```
  kubectl -n flux logs deployment/flux | grep identity.pub | cut -d '"' -f2
```

## Проверка успешного деплоя

Переходим в браузере по ссылкам:
https://grafana.51.250.92.226.nip.io/ - Grafana (login/pass: admin/prom-operator)
https://prometheus.51.250.92.226.nip.io/ - Prometheus
https://kibana.51.250.92.226.nip.io/ - Kibana (визуализация логов)

Если успешно перешлы по всем ссылкам, SSL сертификат в браузере валидный, значит менеджер сертификатов и traefik успешно интегрированы
Проверка MongoDB и RabbitMQ
```
  kubectl -n applications get all
```
В выводе видим развернутые и запущенные сервисы, поды

Проверка Flux
```
  kubectl -n flux get all
```
В выводе видим развернутые и запущенные сервисы, поды



