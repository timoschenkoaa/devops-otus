provider "helm" {
  kubernetes {
    config_path = var.kube_config_path
  }
}

provider "kubernetes" {
  config_path = var.kube_config_path
}

# Создание Namespaces
resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = var.monitoring-ns
  }
}

resource "kubernetes_namespace" "flux" {
  metadata {
    name = var.flux-ns
  }
}

resource "kubernetes_namespace" "logging" {
  metadata {
    name = var.logging-ns
  }
}

resource "kubernetes_namespace" "traefik" {
  metadata {
    name = var.traefik-ns
  }
}

resource "kubernetes_namespace" "certmgr" {
  metadata {
    name = var.certmgr-ns
  }
}

resource "kubernetes_namespace" "applications" {
  metadata {
    name = var.applications-ns
  }
}

# Развертывание Traefik Ingress

resource "helm_release" "traefik" {

  name       = "traefik"
  repository = "https://helm.traefik.io/traefik"
  chart      = "traefik"
  namespace  = kubernetes_namespace.traefik.metadata[0].name

  values = [
    "${file("${path.module}/manifests/traefik/traefik-values.yaml")}",
  ]
  provisioner "local-exec" {
    command = "kubectl apply --validate=false -f manifests/traefik/ingresses.yaml"
  }
}

# Развертывание Certifacate Manager

resource "helm_release" "cert-manager" {

  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  namespace  = kubernetes_namespace.certmgr.metadata[0].name

}
resource "null_resource" "Cert-ClusterIssuer" {
  provisioner "local-exec" {
    command = "kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.5.3/cert-manager.crds.yaml"
  }
}

resource "null_resource" "Certs" {
  depends_on = [
    null_resource.Cert-ClusterIssuer
  ]
  provisioner "local-exec" {
    command = "kubectl apply --validate=false -f ./manifests/cert-manager/"
  }
}

# Развертывание системы логирования EKF
resource "null_resource" "EKF" {
  provisioner "local-exec" {
    command = "kubectl apply -f ./manifests/logging/ -n ${kubernetes_namespace.logging.metadata[0].name}"
  }
}

resource "null_resource" "MongoDB" {
  provisioner "local-exec" {
    command = "kubectl apply -f ./manifests/mongodb/ -n ${kubernetes_namespace.applications.metadata[0].name}"
  }
}

resource "null_resource" "RabbitMQ" {
  provisioner "local-exec" {
    command = "kubectl apply -f ./manifests/rabbitmq/ -n ${kubernetes_namespace.applications.metadata[0].name}"
  }
}

# # Развертывание prometheus grafana
# # Пароли по умолчанию для Grafana  admin/prom-operator
resource "helm_release" "prometheus-operator" {
  name       = "prometheus-operator"
  repository = "https://charts.helm.sh/stable"
  chart      = "prometheus-operator"
  namespace  = kubernetes_namespace.monitoring.metadata[0].name
  values = [
    "${file("${path.module}/manifests/monitoring/prometheus-values.yaml")}",
  ]

  provisioner "local-exec" {
    command = "kubectl apply --validate=false -f ./manifests/monitoring/grafana-dashboards.yaml"
  }

}

resource "null_resource" "FluxCD" {
  provisioner "local-exec" {
    command = "kubectl apply -f manifests/flux/helmrelease.yaml -n ${kubernetes_namespace.flux.metadata[0].name}"
  }
}

resource "helm_release" "flux" {
  name       = "flux"
  repository = "https://charts.fluxcd.io"
  chart      = "flux"
  namespace  = kubernetes_namespace.flux.metadata[0].name
  values = [
    "${file("${path.module}/manifests/flux/flux-values.yaml")}",
  ]
}

resource "helm_release" "flux-helm-operator" {
  name       = "helm-operator"
  depends_on = [
    helm_release.flux
  ]
  repository = "https://charts.fluxcd.io"
  chart      = "helm-operator"
  namespace  = kubernetes_namespace.flux.metadata[0].name
  values = [
    "${file("${path.module}/manifests/flux/helm-operator-values.yaml")}",
  ]
}
