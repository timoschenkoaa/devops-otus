variable "kube_config_path" {
  type        = string
  description = "Путь к файлу конфигурации kubernetes"
}

variable "monitoring-ns" {
  type    = string
  default = "monitoring"
}

variable "logging-ns" {
  type    = string
  default = "logging"
}

variable "traefik-ns" {
  type    = string
  default = "traefik"
}

variable "certmgr-ns" {
  type    = string
  default = "cert-manager"
}

variable "applications-ns" {
  type    = string
  default = "applications"
}

variable "flux-ns" {
  type    = string
  default = "flux"
}
